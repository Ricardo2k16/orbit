// Fill out your copyright notice in the Description page of Project Settings.

#include "Orbit.h"
#include "Planet.h"


// Sets default values
APlanet::APlanet()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	text = CreateDefaultSubobject<UTextRenderComponent>(TEXT("test"));
	text->SetHorizontalAlignment(EHTA_Center);
	text->SetWorldSize(150.0f);
	RootComponent = text;

	rotatorComponent = CreateDefaultSubobject<URotatingMovementComponent>(TEXT("rotator"));
	rotatorComponent->RotationRate = FRotator(0.0f, 80.0f, 0.0f);
	rotatorComponent->bRotationInLocalSpace = false;
}

// Called when the game starts or when spawned
void APlanet::BeginPlay()
{
	Super::BeginPlay();
	text->SetText(Text);
	if (!Target)
	{
		GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Magenta, FString::Printf(TEXT("Object %s has no taget."),
			*(text->Text.ToString())));
		return;
	}
	else
	{
		rotatorComponent->PivotTranslation = Target->GetTransform().GetLocation() - this->GetTransform().GetLocation();
		DrawDebugLine(GetWorld(), this->GetTransform().GetLocation(), Target->GetTransform().GetLocation(), FColor::Red, false);

		GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Magenta, FString::Printf(TEXT("For object %s the target is %s \n and the pivot is: %s"),
			*(text->Text.ToString()),
			*(Target->text->Text.ToString()),
			*(Target->GetTransform().GetLocation().ToString())));
	}

}

// Called every frame
void APlanet::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );
}

