// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "Planet.generated.h"

UCLASS()
class ORBIT_API APlanet : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	APlanet();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	UPROPERTY(EditAnywhere, Category = "Gravitation")
		APlanet* Target;

	UPROPERTY(EditAnywhere, Category = "Initialize")
		FText Text;

	UTextRenderComponent* text;

	UPROPERTY(EditAnywhere, Category = "Gravitation")
		URotatingMovementComponent* rotatorComponent;
	
};
